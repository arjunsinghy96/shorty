const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const crypto = require('crypto');
const router = require('./router.js');

var shorts = {'g': 'https://www.google.com',
              'f': 'https://www.facebook.com'}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use('/', router);

app.use('/static', express.static('public'));

app.listen(3000, function(){
    console.log('Listening to 3000');
});
