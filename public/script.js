var data = {short_url: "nothing yet"};

var list_app = new Vue({
    el: '#short-url',
    data: data
});

$('#url-form').submit(function(){
    var formData = $('#url-form').serializeArray();
    var postData = {};
    for(var i=0; i<formData.length; i++){
        postData.url = formData[i].value;
    }
    $.post('/', postData, function(res_data, status){
        data.short_url = res_data.short_url;
    });
    return false;
});
