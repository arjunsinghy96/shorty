const express = require('express');
const crypto = require('crypto');
const Shorts = require('./db').Shorts
const router = express.Router();

router.route("/")
    .get(function(req, res){
        res.sendFile(__dirname + '/public/index.html');
    })
    .post(function(req, res){
        var response = {};
        Shorts.findOne({url: req.body.url}, function(err, data){
            if(err){
                response = {"error": true, "message": "some error occured"};
                res.json(message);
            }
            else{
                console.log(data);
            }
            if(data !== null){
                response = {"error": false, "short_url": data.url_hash.slice(0, 6)}; 
                res.json(response);
            }
            else{
                console.log(req.body.url);
                var short_hash = crypto.createHash('sha256')
                                 .update(req.body.url)
                                 .update(process.env.HASH_KEY)
                                 .digest('hex');
                var db = new Shorts();
                db.url = req.body.url;
                db.url_hash = short_hash;
                console.log(db);
                db.save(function(err){
                    if(err){
                        response = {"error": true, "message": "some error occured"};
                    }
                    else{
                        response = {"error": false, "short_url": short_hash.slice(0, 6)};
                    }
                    res.json(response);
                });
            }
        });
    }); 
router.route("/:path")
    .get(function(req, res){
        console.log(req.params.path);
        var re = new RegExp("^" + req.params.path, 'i');
        Shorts.findOne({url_hash: {$regex: re} }, function(err, data){
            if(err){
                res.json({"error": true, "message": "URI does not exist in our database"});
            }
            else if(data !== null){
                res.redirect(301, data.url);
            }
            else{
                res.json({"error": true, "message": "URI does not exist in our database"});
            }
        });
    });

module.exports = router;
