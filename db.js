const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/shorty')

var Schema = mongoose.Schema;

var shortySchema = new Schema(require('./shorty.json'));

exports.Shorts = mongoose.model('shorty_urls', shortySchema);
