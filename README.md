# shorty

`shorty` is a expressJS application to compress long urls sending them is easy.

When visited the short url `shorty` redirects you to the original webpage.

### Dependencies

- Node
- Express
- MongoDB
- body-parser

or you can run `npm install` to install the dependencies.


### Run shorty locally

- Clone this repository to your local machine.
- Run `cd shorty`
- Run `node server.js`
- shorty will be available at https://localhost:3000/
